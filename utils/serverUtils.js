
module.exports = {
    logUrl: (req, res, next) => {
        console.log('o==> o==> o==>', req.path)
        next()
    },

    advancedLogUrl: (req, res, next) => {
        console.log(' ____  ___ _____\n //_/ //_/  //  //_/\n//   // /  //  //', req.path)
        next()
    },

    logError: (err) => {
        console.error('   ____ _____ _____\n  //__  //__/ //__/\n //___ // \\\\ //  \\\\     ', err)
    },

    greet: (port) => {
        console.log('               _    ___  ___  _  _     _ _____\n ************',
            '//_/ //_/ //   //_/     //  //    ************\n************',
            '// / // / //__ //  \\    //  //    ************\n',
            'Server listening on port', port)
    },
}
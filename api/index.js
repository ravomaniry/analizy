const express = require('express')
const router = express.Router()

const HTTPError = require('../error')

router.use('/spreadsheet/', require('./spreadsheet'))
router.use('/demoapp/', require('./demoapp'))


router.use(HTTPError.notFound)


module.exports = router

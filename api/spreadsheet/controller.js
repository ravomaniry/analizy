const readRange = require('./readRange')
const httpResponse = require('../../responses')
const httpError = require('../../error')

const errCodesMap = {
    authError: 500,
    spreadsheetError: 400,
}

function read(req, res) {
    if (req.query.id && req.query.range) {
        readRange(req.query)
            .then(data => {
                httpResponse.success(res, data)
            })
            .catch(err => {
                httpError.custom(res, errCodesMap[err.code] || 500, err)
            })
    }
    else {
        httpError.missingParams(res, 'id / range')
    }
}


module.exports = {
    read
}

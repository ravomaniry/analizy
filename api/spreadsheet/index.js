const router = require('express').Router()

const controller = require('./controller')

router.get('/read', controller.read)

router.use(require('../../error').notFound)


module.exports = router

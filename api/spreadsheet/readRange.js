const { google } = require('googleapis')
const sheetsApi = google.sheets('v4')
const createConnection = require('./connection')



const readRanges = function ({ id, range }) {
    return new Promise((resolve, reject) => {
        console.log('====', 'Read range:', id, range)

        if (!id || !range) {
            console.log('========', 'Missing params', id, range)

            return reject({
                code: 'MissingField',
                message: 'Spreadsheet id and ranges are required!'
            })
        }

        createConnection()
            .then(auth => {
                const params = {
                    auth,
                    range,
                    spreadsheetId: id,
                }

                sheetsApi.spreadsheets.values.get(params, function (err, data) {
                    console.log('============', 'Read:\n         Error:',
                        err ? err : 'No error\n',
                        data ? '         Data is ok :-)' : '         No data')

                    if (err)
                        reject({ code: 'spreadsheetError', message: err.message || err })

                    resolve(data.data.values)
                })
            })
            .catch(err => {
                console.log('========', 'Auth errror', err.message || err)
                reject({ code: 'authError', message: err.message || err })
            })
    })
}


module.exports = readRanges

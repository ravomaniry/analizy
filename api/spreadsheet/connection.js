/*--
  Connexion should be done once
  When connexion is established,  read ranges without creating new connexion
*/
const fs = require('fs');
const authorize = require('./googleauth');
const path = require('path');

//-- Connection creation
const createConnection = function () {
    return new Promise((resolve, reject) => {
        fs.readFile(path.join(__dirname, 'credentials/client_secret.json'), 'utf8', function (err, content) {
            if (err) {
                console.log('Error loading client secret file: ' + err);
                reject(err);
            }
            authorize(JSON.parse(content), function (oauth2Client) {
                resolve(oauth2Client);
            });
        });
    });
}

module.exports = createConnection;
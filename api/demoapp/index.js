const router = require('express').Router()

const httpError = require('../../error')
const csvController = require('./csv.controller')


router.get('/data/:filename', csvController)



router.use(httpError.notFound)


module.exports = router

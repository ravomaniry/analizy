const fs = require('fs')
const path = require('path')

const httpResponse = require('../../responses')
const httpError = require('../../error')
const serverUtilss = require('../../utils/serverUtils')


module.exports = (req, res) => {
    const { filename } = req.params

    fs.readFile(path.join(__dirname, 'sampleData', filename + '.csv'), 'utf8', (err, content) => {
        if (err) {
            if (err.code === 'ENOENT')
                httpError.notFound(req, res)
            else
                httpError.serverError(res, 'Could not read ' + filename + '.csv', err.stack)

            serverUtilss.logError('File reading error' + err.stack)
        }
        else
            httpResponse.success(res, { content })
    })
}

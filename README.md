# ANALIZY DEPLOYMENT SERVER #

### Example ###

`http://localhost:8080/api/spreadsheet?id=1c76H8O-W1D6o_7AvSk0kiOKL47zdI4f4j5r8nMUA5dA&range=Sheet1!A2:B10`  
`http://localhost:8080/api/demoapp/data/sales`

**id** : the spreadsheet id  
**range**: uri encoded cells adress like `encodeURIComponent('Sheet1!A2:B10')`

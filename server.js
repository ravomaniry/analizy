const express = require('express')

const serverUtils = require('./utils/serverUtils')


const app = express()
const port = process.env.PORT || 8080

app.use(require('cors')())
app.use(serverUtils.logUrl)


require('./Routes')(app)


app.listen(port)
serverUtils.greet(port)

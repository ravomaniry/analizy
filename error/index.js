const responses = require('../responses')


const errors = {
    notFound: (req, res) => {
        responses.error(res, {
            message: 'Page not found: ' + req.path,
            code: 'notFound',
        }, 404)
    },

    custom: (res, statusCode, error) => {
        responses.error(res, error, statusCode || 400)
    },

    missingParams: (res, params) => {
        responses.error(res, {
            message: 'Missing parameter(s): ' + (typeof (params) === 'string' ? params : JSON.stringify(params)),
            code: 'missingParams'
        }, 400)
    },

    serverError: (res, message, stack) => {
        responses.error(res, {
            message,
            stack,
            code: 'serverError',
        }, 500)
    }
}


module.exports = errors;
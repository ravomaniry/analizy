const responses = {
    success: (res, data, statusCode) => {
        return res.status(statusCode || 200).json(data)
    },

    error: function (res, err, statusCode) {
        return res.status(statusCode || 500).json({
            code: err.code || null,
            message: err.message || '',
            stack: err.stack || null,
        })
    }
}


module.exports = responses 

module.exports = {
   "env": {
      "node": true,
      "es6": true
   },
   "extends": "eslint:recommended",
   "parserOptions": {
      "ecmaVersion": 2017
   },
   "rules": {
      "linebreak-style": [
         "error",
         "windows"
      ],
      "semi": 0,
      "no-console": 0,
      "no-unused-vars": 1
   }
};

const path = require('path')
const serveStatic = require('serve-static')

module.exports = function (app) {
    app.get('/googlee400912478ca15e7.html', (req, res) => {
        res.sendFile(path.join(__dirname, 'public', 'seo', 'googlee400912478ca15e7.html'))
    })

    app.get('/', (req, res) => {
        res.sendFile(path.join(__dirname, 'public', 'index.html'))
    })

    app.use('/api', require('./api'))

    app.use(serveStatic(path.join(__dirname, 'public')))

    app.use(require('./error').notFound)
}
